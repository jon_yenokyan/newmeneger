<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const CREATED = 1;
    const ASSIGNED = 2;
    const INPROGRESS = 3;
    const DONE = 4;

    protected $fillable = [
        'title', 'description', 'user_id', 'user_assign_id', 'status'
    ];

    public $timestamps = true;


    public function getStatusNameAttribute()
    {
        return array_search($this->status, [
            'CREATED' => self::CREATED,
            'ASSIGNED' => self::ASSIGNED,
            'INPROGRESS' => self::INPROGRESS,
            'DONE' => self::DONE
        ]);
    }

    public function getAssignedUserAttribute($value)
    {
        if ($this->user_assign_id == null) {
            return null;
        }
        return User::find($this->user_assign_id)->name;
    }

    public function getCreatedUserAttribute($value)
    {
        return User::find($this->user_id)->name;
    }


    public function scopeStatuses()
    {
        return [
            self::CREATED => 'CREATED',
            self::ASSIGNED => 'ASSIGNED',
            self::INPROGRESS => 'INPROGRESS',
            self::DONE => 'DONE'
        ];
    }


}
