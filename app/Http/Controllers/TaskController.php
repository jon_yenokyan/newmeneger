<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $request->get('title');
        $role = Auth::user()->role;
        $tasks = Task::query()
            ->when($title, function ($query, $whereLike) {
                return $query->where('title', 'LIKE', '%' . $whereLike . '%');
            })
            ->where(function ($query) use ($role) {
                if ($role == config('constants.role')['meneger']) $query->where('user_id', Auth::id());
            })
            ->get();

        return view('tasks.index', compact('tasks', 'role', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('hasMeneger'))
            abort(403);

        $users = User::where('role', config('constants.role')['developer'])->get();

        return view('tasks.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
            ]);
            //Add auth user id
            $request->merge([
                'user_id' => Auth::user()->getKey()
            ]);

            Task::create($request->all());

            return response()->json([
                'data' => [
                    'status' => 200,
                    'message' => 'Task updated successfully',
                ],
            ], 200);

        } catch (Exception $e) {
            return response()->json([
                'data' => [
                    'status' => 500,
                    'message' => 'Error',
                ],
            ], 500);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        if (Auth::user()->can('taskShow' , $task))
            abort(403);

        return view('tasks.show', compact('task'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        if (!Auth::user()->can('hasCreatedOrAssigned', $task))
            abort(403);

        $users = User::query()->where('role', config('constants.role')['developer'])->get();
        return view('tasks.edit', compact('task', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        if (!Auth::user()->can('hasCreatedOrAssigned', $task))
            throw new Exception('permission denied');

        try {
            if (Auth::user()->can('hasCreated', $task))
                $request->validate([
                    'title' => 'required',
                    'description' => 'required',
                    'status' => 'required',
                ]);
            elseif (Auth::user()->can('hasAssigned', $task))
                $request->validate([
                    'status' => 'required',
                ]);

            $task->update($request->all());

            return response()->json([
                'data' => [
                    'status' => 200,
                    'message' => 'Task updated successfully',
                ],
            ], 200);

        } catch (Exception $e) {
            return response()->json([
                'data' => [
                    'status' => 500,
                    'message' => 'Error',
                ],
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        try {
            if (!(Auth::user()->can('hasCreated', $task)))
                throw new Exception('permission denid');

            $task->delete();

            return response()->json([
                'data' => [
                    'status' => 200,
                    'message' => 'Task Deleted successfully',
                ],
            ], 200);

        } catch (Exception $e) {
            return response()->json([
                'data' => [
                    'status' => 500,
                    'message' => 'Error',
                ],
            ], 500);
        }
    }
}
