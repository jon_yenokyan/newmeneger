@extends('layouts.app')

@section('content')
    <div class="row" style="margin-bottom: 15px">
        <div class="col-lg-12 margin-tb">
            <form method="get">
                <div class="form-group">
                    <div class="col-md-3">
                        <label>Title</label>
                        <input type="text" name="title" value="{{$title}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <label>Search</label>
                        <input type="submit" value="Search">
                    </div>
                </div>
            </form>
            @if($role == 'meneger')
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('tasks.create') }}"> Create New Task</a>
                </div>
            @endif
        </div>
    </div>
    <div id="form-messages" class="alert success" role="alert" style="display: none;"></div>
    <table class="table table-bordered">
        <tr>
            <th>Title</th>
            <th>Created User</th>
            <th>Assigned User</th>
            <th>Created Time</th>
            <th>Status</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($tasks as $task)
            <tr>
                <td>{{ $task->title }}</td>
                <td>{{ $task->createdUser }}</td>
                <td>{{ $task->assignedUser }}</td>
                <td>{{ $task->created_at }}</td>
                <td>{{ $task->statusName }}</td>
                <td>
                    <a class="btn btn-info" href="{{ route('tasks.show',$task->id) }}">Show</a>
                    @can('hasCreatedOrAssigned', $task)
                        <a class="btn btn-primary" href="{{ route('tasks.edit',$task->id) }}">Edit</a>
                    @endcan
                    @can('hasCreated', $task)
                        <form class="delete-task">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="id" value="{{ $task->id }}">
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    @endcan
                </td>
            </tr>
        @endforeach
    </table>
@endsection
